#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    int n = 5;
    int i;

    int *v2 = (int*) malloc(n * sizeof(int));

    printf("&v2 = %p, v2 = %p\n", &v2, v2);
    for (i = 0; i < n; i++) {
        v2[i] = i;
        printf("&v2[%d] = %p, v2[%d] = %d\n", i, &v2[i], i, v2[i]);
    }

    free(v2);
    v2 = NULL;

    return 0;
}