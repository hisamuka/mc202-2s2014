#include <stdio.h>
#include <stdlib.h>


void PrintDoubleVector(double *v, int n) {
    int i;

    for (i = 0; i < n; i++)
        printf("v[%d] = %.2lf\n", i, v[i]);
    puts("");
}


double *CreateDoubleVector(int n) {
    return ((double*) malloc(n * sizeof(double)));
}


void DestroyDoubleVector(double **v) {
    free(*v);
    *v = NULL;
}


void ScalarToDoubleVector(double value, double *v, int n) {
    int i;

    for (i = 0; i < n; i++)
        v[i] += value;
}


int main() {
    int n; // vector size
    int i;
    double *v;

    scanf("%d", &n);
    v = CreateDoubleVector(n);

    for (i = 0; i < n; i++)
        scanf("%lf", &v[i]);

    PrintDoubleVector(v, n);
    ScalarToDoubleVector(10.0, v, n);
    PrintDoubleVector(v, n);

    DestroyDoubleVector(&v);

    return 0;
}