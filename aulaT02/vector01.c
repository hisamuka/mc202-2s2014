#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[]) {
    
    int v[5] = {1,2,3,4,5};
    int i;

    printf("&v = %p, v = %p\n", &v, v);
    for (i = 0; i < 5; i++) {
        printf("&v[%d] = %p, v[%d] = %d\n", i, &v[i], i, v[i]);
    }
    puts("");

    
    for (i = 0; i < 5; i++) {
        printf("(v+%d) = %p, *(v+%d) = %d\n", i, (v+i), 
            i, *(v+i));
    }
    puts("");


    return 0;
}