#ifndef _PILHA_H_
#define _PILHA_H_

#include <stdio.h>
#include <stdlib.h>

typedef struct _pilha {
    int topo;
    int MAX_SIZE;
    int *val; // vetor de elementos
} Pilha;


/**************** HEADERS ****************/
Pilha *CriarPilha(int MAX_SIZE);
void DestruirPilha(Pilha **P);
int PilhaVazia(Pilha *P);
int PilhaCheia(Pilha *P);
int Push(Pilha *P, int elem);
int Pop(Pilha *P, int *elem);
int ConsultarPilha(Pilha *P, int *elem);
/*****************************************/

#endif