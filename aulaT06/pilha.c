#include "pilha.h"

Pilha *CriarPilha(int MAX_SIZE) {
    Pilha *P = (Pilha*) calloc(1, sizeof(Pilha));
    
    P->val = (int*) calloc(MAX_SIZE, sizeof(int));    
    P->MAX_SIZE = MAX_SIZE;
    P->topo = 0;

    return P;
}


void DestruirPilha(Pilha **P) {
    Pilha *Paux = *P;
    free(Paux->val); // desalocando o vetor
    free(Paux); // desalocando a struct P
    *P = NULL;
}


int PilhaVazia(Pilha *P) {
    return (P->topo == 0);
}

int PilhaCheia(Pilha *P) {
    return (P->topo == P->MAX_SIZE);
}


int Push(Pilha *P, int elem) {
    if (!PilhaCheia(P)) { // = if (PilhaCheia == 0)
        P->val[P->topo] = elem; // empilha o elem em P
        P->topo++;
        return 1; // empilhou
    }
    else { // Pilha esta cheia
        printf("Warning: Pilha esta cheia\n");
        return 0; // nao empilhou
    }
}

/* Desempilha e retorna o elemento desempilhado
em *elem
Se o retorno for 1, o elemento foi desempilhado
Se 0, o elemento NÃO foi desempilhado
 */
int Pop(Pilha *P, int *elem) {
    if (PilhaVazia(P)) {
        printf("Warning: Pilha esta vazia\n");
        return 0; // nao desempilhou
    }
    else { // ha pelo menos um elemento na Pilha
        // decrementa P->topo e acessa sua nova posição decrementada
        *elem = P->val[--P->topo];
        return 1; // desempilhou
    }
}


/* Retorna o elemento do topo da pilha em *elem,
mas, não desempilha
Se o retorno for 1, o elemento foi desempilhado
Se 0, o elemento NÃO foi desempilhado
*/
int ConsultarPilha(Pilha *P, int *elem) {
    if (PilhaVazia(P)) {
        printf("Warning: Pilha esta vazia\n");
        return 0; // nao desempilhou
    }
    else { // ha pelo menos um elemento na Pilha
        // acessa a posição P->topo-1, sem decrementar P->topo 
        *elem = P->val[P->topo-1];
        return 1; // desempilhou
    }
}















