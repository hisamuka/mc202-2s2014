#include "pilha.h"

// Para compilar
// gcc -c pilha.c -I.
// gcc test_pilha.c -o test -I. pilha.o

void PrintVector(int *v, int n) {
    int i;
    for (i = n-1; i >= 0; i--)
        printf("[%d] = %d\n", i, v[i]);
    puts("");
}


int main(int argc, char *argv[]) {
    int elem;

    Pilha *P = CriarPilha(5);
    
    Pop(P, &elem);

    Push(P, 1);
    PrintVector(P->val, P->topo);
    Push(P, 2);
    PrintVector(P->val, P->topo);
    Push(P, 3);
    PrintVector(P->val, P->topo);
    Pop(P, &elem);
    PrintVector(P->val, P->topo);
    printf("elem = %d\n", elem);
    
    Push(P, 4);
    PrintVector(P->val, P->topo);
    Push(P, 5);
    PrintVector(P->val, P->topo);
    Push(P, 6);
    PrintVector(P->val, P->topo);
    Push(P, 7);
    PrintVector(P->val, P->topo);




    DestruirPilha(&P);

    return 0;
}