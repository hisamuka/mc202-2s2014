Para compilar o student.c, nós fazemos o seguinte:

gcc -c student.c

A flag -c significa que o compilador (gcc) espera que você passe um arquivo source (.c), não importando/esperando que ele possua o programa principal (main);

O resultado da compilação acima é um arquivo chamado student.o
Portanto, a flag -c, cria um arquivo com extensão .o de mesmo nome do arquivo .c que foi compilado;

Em student.c, nós incluímos/importamos o arquivo de cabeçalhos student.h.
Se o arquivo de cabeçalho estiver no mesmo diretório do arquivo .c a ser compilado, o qual importa tal header, não precisamos informar ao compilador qual o diretório que se encontram os arquivos de cabeçalho.