Dado que os arquivos de cabeçalhos (*.h) não estão no mesmo diretório do arquivo source (*.c) que você quer compilar, você PRECISA INFORMAR AO COMPILADOR ONDE O ARQUIVO SE ENCONTRA, ou seja, qual é o seu diretório

gcc -c student.c -o obj/student.o -I./include

A flag -Iinclude informa ao compilador que há arquivos de cabeçalho no diretorio "include", que possam ser usados pelos meus .c a serem compilados;

Obs: arquivos .h que estão no mesmo diretório do arquivo .c a ser compilado continuam funcionando;
