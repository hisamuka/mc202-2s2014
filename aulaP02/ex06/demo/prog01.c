#include "student.h"
#include "common.h"

int main() {
    Student *st = CreateStudent2("Ted Mosby", 30, 123456);
    PrintStudent(st);
    DestroyStudent(&st);

    int *v = AllocIntVector(5);
    free(v);

    return 0;
}