/* Arquivo de Headers
*  Contém definições de tipos, estruturas,
*  e cabeçalhos de funções.
*
* ESTE ARQUIVO NÃO É COMPILÁVEL, APENAS É REFERENCIADO
* OU INCLUSO
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h> // contém strcpy, strcmp, ...

typedef struct _student {
    char name[50];
    int age;
    int ra;
} Student;


// FUNTION HEADERS
Student *CreateStudent();
Student *CreateStudent2(char *name, int age, int ra);
void PrintStudent(Student *st);
int FindYoungestStudent(Student **st);
void DestroyStudent(Student **st);













