Dado a estrutura de pastas deste exemplo, temos o seguinte comando de compilação:

gcc -c src/student.c -o obj/student.o -I./include

Ou seja, o compilador quer compilar o código student.c, que está no diretório src, gerando um arquivo objeto (compilado) de nome student.o em obj.
Além disso, ele olhará para o diretório include, procurando por arquivos .h que possa vir a utilizar nos arquivos a serem compilados;

Obs: arquivos .h que estão no mesmo diretório do arquivo .c a ser compilado continuam funcionando;