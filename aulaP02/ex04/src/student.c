/* Arquivo que contém os bodies (corpo das funções)
* definidas no respectivo .h
*
* Não é necessário (obrigatório) implementar as funções
* definidas no .h
* Porém, é recomendável
*
* ESTE ARQUIVO É COMPILÁVEL
*/

// Importa tudo o que foi definido no student.h
#include "student.h"

Student *CreateStudent() {
    Student *st = (Student*) calloc(1, sizeof(Student));
    return (st);
}

Student *CreateStudent2(char *name, int age, int ra) {
    Student *st = CreateStudent(); // aloca na heap o espaço de 1 Student
    strcpy(st->name, name);
    st->age = age;
    st->ra = ra;

    return (st);
}

void PrintStudent(Student *st) {
    printf("Name: %s\n", st->name);
    printf("Idade: %d\n", st->age);
    printf("RA: %d\n\n", st->ra);
}

void DestroyStudent(Student **st) {
    Student *aux = *st;
    free(aux);
    *st = NULL;
}