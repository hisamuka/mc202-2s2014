#include "student.h"

int main() {
    Student *st = CreateStudent2("Ted Mosby", 30, 123456);
    
    PrintStudent(st);
    
    DestroyStudent(&st);

    return 0;
}