Agora, vamos supor que você criou um programa (com main) que usa as funções e definições já feitas em student.h e students.c;

Para usar student, você precisa do student.h e do respectivo arquivo objeto já compilado (student.o)

gcc -c src/student.c -o obj/student.o -I./include
gcc demo/prog01.c -o prog.exe -I./include obj/student.o

O primeiro comando compila o student.c, gerando o arquivo objeto.

O segundo comando compila o arquivo prog01.c (possui a main), que está no diretório "demo", gerando um arquivo executável chamado "prog.exe".
Além disso, como "prog.c" usa as funções de student, precisamos informar ao compilador onde o arquivo student.h se encontra (-I./include), e precisamos passar o arquivo objeto (compilado) de student (student.o) para que de fato possamos usar as funções implementadas.

Obs: não é necessário o arquivo student.c para compilar prog01.exe, basta que você passe seu arquivo header (student.h) e seu arquivo objeto (student.o);