Uma outra forma de compilação:

gcc -c student.c -o STUDENTS.o
gcc -c student.c -o obj/abacaxi

O compilador (gcc) vai compilar o arquivo student.c (que não possui uma main definida, por isso usamos o -c), e vai gerar o arquivo objeto (arquivo compilado) com o nome STUDENTS.o

Portando, a flag -o significa "output", ou seja, estamos definindo um novo nome para o nosso arquivo a ser compilado.

Obs1: o nome dado pode ser qualquer coisa
Obs2: a mesma regra/lógica de importar arquivos .h, visto no ex01, é adotado aqui;