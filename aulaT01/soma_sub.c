#include <stdio.h>

/*  A função retorna a soma de 2 inteiros,
e a subtração de ambos é "retornada" em sub
*/
int soma_sub1(int a, int b, int *sub) {
    int soma = a+b;
    *sub = a-b;

    return soma;
}

void soma_sub2(int a, int b, int *soma, int *sub) {
    *soma = a+b;
    *sub  = a-b;
}


int main() {
    int a = 10;
    int b = 5;
    int soma, sub;

    soma = soma_sub1(a, b, &sub);
    printf("a = %d, b = %d, soma = %d, sub = %d\n", a, b, soma, sub);

    soma_sub2(a, b, &soma, &sub);
    printf("a = %d, b = %d, soma = %d, sub = %d\n", a, b, soma, sub);

    return 0;
}