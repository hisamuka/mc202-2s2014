#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char name[32];
    int age;
} Student;


int main() {
    Student *p1;
    p1 = (Student*) calloc(1, sizeof(Student));

    strcpy(p1->name, "Ted Mosby"); // vem da string.h
    p1->age = 30;
    
    printf("p1.name: %s\n", p1->name);
    printf("p1.age: %d\n\n", p1->age);

    free(p1);
    p1 = NULL;

    return 0;
}