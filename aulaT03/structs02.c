#include <stdio.h>
#include <stdlib.h>

struct _student {
    char name[8];
    int age;
};
typedef struct _student Student;


int main() {
    struct _student p1 = {"Ted", 30};
    Student p2 = {.name = "Barney", .age = 31};

    printf("&p1 = %p\n", &p1);
    printf("&p1.name = %p, p1.name: %s\n", 
        &p1.name, p1.name);
    printf("&p1.age = %p, p1.age: %d\n\n", 
        &p1.age, p1.age);

    printf("p2.name: %s\n", p2.name);
    printf("p2.age: %d\n\n", p2.age);


    return 0;
}