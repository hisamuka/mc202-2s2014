#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct _student {
    char name[32];
    int age;
};


int main() {
    struct _student p1 = {"Ted", 30};
    struct _student p2 = {.name = "Barney", .age = 31};
    struct _student p3;
    strcpy(p3.name, "Marshall");
    p3.age = 31;
    struct _student p4;
    p4 = p1; // só funciona com structs estáticas de mesmo tipo

    printf("&p1 = %p\n", &p1);
    printf("&p1.name = %p, p1.name: %s\n", 
        &p1.name, p1.name);
    printf("&p1.age = %p, p1.age: %d\n\n", 
        &p1.age, p1.age);

    printf("p2.name: %s\n", p2.name);
    printf("p2.age: %d\n\n", p2.age);

    printf("p3.name: %s\n", p3.name);
    printf("p3.age: %d\n\n", p3.age);

    printf("p4.name: %s\n", p4.name);
    printf("p4.age: %d\n\n", p4.age);




    return 0;
}