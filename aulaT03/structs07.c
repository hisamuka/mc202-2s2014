#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char name[32];
    int age;
} Student;


int main() {
    Student *alunos = (Student*) calloc(3, sizeof(Student));

    strcpy(alunos[0].name, "Ted Mosby"); 
    alunos[0].age = 30;

    strcpy(alunos[1].name, "Barney Stinson");
    alunos[1].age = 31;

    strcpy(alunos[2].name, "Marshall");
    alunos[2].age = 32;

    int i = 0;
    for (i = 0; i < 3; i++) {
        printf("Name: %s\n", alunos[i].name);
        printf("Age: %d\n\n", alunos[i].age);
    }

    // OUTRO JEITO DE FAZER
    // int i = 0;
    // for (i = 0; i < 3; i++) {
    //     printf("Name: %s\n", (alunos+i)->name);
    //     printf("Age: %d\n\n", (alunos+i)->age);
    // }

    free(alunos);
    alunos = NULL;

    return 0;
}