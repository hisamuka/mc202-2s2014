#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char name[32];
    int age;
} Student;


int main() {
    int i;
    Student **alunos = (Student**) calloc(3, sizeof(Student*));

    for (i = 0; i < 3; i++) {
        alunos[i] = (Student*) calloc(1, sizeof(Student));
    }

    strcpy(alunos[0]->name, "Ted Mosby"); 
    alunos[0]->age = 30;

    strcpy(alunos[1]->name, "Barney Stinson");
    alunos[1]->age = 31;

    strcpy(alunos[2]->name, "Marshall");
    alunos[2]->age = 32;
 
    for (i = 0; i < 3; i++) {
        printf("Name: %s\n", alunos[i]->name);
        printf("Age: %d\n\n", alunos[i]->age);
    }

    for (i = 0; i < 3; i++)
        free(alunos[i]);
    free(alunos);    
    alunos = NULL;

    return 0;
}