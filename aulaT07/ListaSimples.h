#ifndef LISTA_SIMPLES_H_
#define LISTA_SIMPLES_H_

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>

typedef struct no_lista_simples {
    int info;
    struct no_lista_simples *prox;
} ListaSimples, NoListaSimples;

char ListaSimplesVazia(ListaSimples *inicio);

void InsereInicioListaSimples(ListaSimples **inicio, int info);

void ImprimeListaSimples(ListaSimples *inicio);

void InsereFimListaSimples(ListaSimples **inicio, int info);

int  RemoveInicioListaSimples(ListaSimples **inicio);

int  RemoveFimListaSimples(ListaSimples **inicio);

NoListaSimples *BuscaElementoListaSimples(ListaSimples *inicio, int info);

ListaSimples *ConcatenaListasSimples(ListaSimples *inicio1, ListaSimples *inicio2);

void DestroyListaSimples(ListaSimples **inicio);

void InsereElementoListaSimplesOrdenada(ListaSimples **inicio, int info);

void RemoveElementoListaSimplesOrdenada(ListaSimples **inicio, int info);

ListaSimples *UneListasSimples(ListaSimples *inicio1, ListaSimples *inicio2);

ListaSimples *InterseccionaListasSimples(ListaSimples *inicio1, ListaSimples *inicio2);

#endif
