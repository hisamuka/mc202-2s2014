#include "ListaSimples.h"

char ListaSimplesVazia(ListaSimples *inicio)
{
    if (inicio==NULL)
      return 1;
    else
      return 0;
}

void InsereInicioListaSimples(ListaSimples **inicio, int info)
{
    NoListaSimples *q = (NoListaSimples *)calloc(1,sizeof(NoListaSimples));

    q->info = info;
    q->prox = NULL;

    if (ListaSimplesVazia(*inicio)){
      *inicio=q;
    }else{
      q->prox = *inicio;
      *inicio  = q;
    }
}

void ImprimeListaSimples(ListaSimples *inicio)
{
  ListaSimples *p=inicio;

  while (p!=NULL) {
      printf("%d ",p->info);
      p = p->prox;
  }
  printf("\n");
}

void InsereFimListaSimples(ListaSimples **inicio, int info)
{
    NoListaSimples *q = (NoListaSimples *)calloc(1,sizeof(NoListaSimples));

    q->info = info;
    q->prox = NULL;

    if (ListaSimplesVazia(*inicio)){
      *inicio=q;
    }else{
      NoListaSimples *p=*inicio;
      while (p->prox!=NULL){
         p = p->prox;
      }
      p->prox = q;
    }
}

int RemoveInicioListaSimples(ListaSimples **inicio)
{
    if (ListaSimplesVazia(*inicio)){
      printf("Erro: Lista Vazia");
      exit(-1);
    }else{
      NoListaSimples *p = *inicio;
      *inicio  = p->prox;
      int info = p->info;
      free(p);
      return(info);
    }
}

int RemoveFimListaSimples(ListaSimples **inicio)
{
    if (ListaSimplesVazia(*inicio)){
      printf("Erro: Lista Vazia");
      exit(-1);
    }else{
      NoListaSimples *p = *inicio, *pred_p;
      while (p->prox != NULL){
        pred_p = p;
        p = p->prox;
      }
      int info       = p->info;
      if (p==*inicio){
         *inicio=NULL;
      } else {
         pred_p->prox = NULL;
      }
      free(p);
      return(info);
    }
}

NoListaSimples *BuscaElementoListaSimples(ListaSimples *inicio, int info)
{
    NoListaSimples *p=inicio;

    while ((p != NULL)&&(p->info != info)){
        p = p->prox;
    }

    return(p);
}

ListaSimples *ConcatenaListasSimples(ListaSimples *inicio1, ListaSimples *inicio2)
{
    ListaSimples   *inicio3=NULL;
    NoListaSimples *p;

    p = inicio1;
    while(p != NULL){
       InsereFimListaSimples(&inicio3,p->info);
       p = p->prox;
    }
    p = inicio2;
    while(p != NULL){
       InsereFimListaSimples(&inicio3,p->info);
       p = p->prox;
    }

    return(inicio3);
}

void DestroyListaSimples(ListaSimples **inicio)
{
    NoListaSimples *p=*inicio, *q;

    while (p != NULL){
        q = p->prox;
        free(p);
        p = q;
    }
    *inicio = NULL;
}

void InsereElementoListaSimplesOrdenada(ListaSimples **inicio, int info)
{
    NoListaSimples *p, *q, *pred_p=NULL;

    q = (NoListaSimples *)calloc(1,sizeof(NoListaSimples));
    q->info = info;
    q->prox = NULL;

    p = *inicio;
    while ((p != NULL)&&(p->info < info)){
       pred_p = p;
       p = p->prox;
    }
   if (pred_p != NULL){
    pred_p->prox = q;
   } else {
    *inicio = q;
   }
   q->prox = p;
}


void RemoveElementoListaSimplesOrdenada(ListaSimples **inicio, int info)
{
    /* Exercício */
}

ListaSimples *UneListasSimples(ListaSimples *inicio1, ListaSimples *inicio2)
{

    /* Exercício */

}

ListaSimples *InterseccionaListasSimples(ListaSimples *inicio1, ListaSimples *inicio2)
{

    /* Exercício */

}





