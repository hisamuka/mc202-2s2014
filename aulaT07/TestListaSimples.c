#include "ListaSimples.h"
#include "ListaSimples.c"

/* Conjuntos e sequências de números (letras) são exemplos de tipos de dados abstratos, cujos elementos poderiam ser armazenados em um vetor, caso soubéssemos o número máximo de elementos. No entanto, conjuntos e sequências são tipos dinâmicos, que podem aumentar e reduzir de tamanho por inserção e remoção de elementos. Neste caso os elementos são armazenados em uma lista ligada por apontadores de memória. A lista será uma fila (First-In-First-Out - FIFO) se as inserções forem no final e as remoções forem no início. A lista será uma pilha (Last-In-First-Out - LIFO) se as inserções e remoções forem no início. Uma lista pode ainda ser:


   simples (ligada em uma única direção, como neste código)


   dupla (ligada em duas direções)


   circular (o último elemento se liga ao primeiro no caso simples, e
             no caso duplo, ambos primeiro e último elementos se ligam)


Exemplos de operações em lista são: inserção no início/fim, remoção no início/fim, impressão, destruição, busca de um dado elemento, remoção de um dado elemento, concatenação de listas, união de listas, intersecção de listas, etc.

*/


int main()
{
  ListaSimples *inicio1=NULL, *inicio2=NULL, *inicio3=NULL;
  int info;

  InsereInicioListaSimples(&inicio1,10);
  ImprimeListaSimples(inicio1);
  InsereFimListaSimples(&inicio1,20);
  ImprimeListaSimples(inicio1);
  info=RemoveFimListaSimples(&inicio1);
  printf("Removido do final: %d\n",info);
  ImprimeListaSimples(inicio1);
  InsereFimListaSimples(&inicio1,20);
  ImprimeListaSimples(inicio1);
  info=RemoveInicioListaSimples(&inicio1);
  printf("Removido do inicio: %d\n",info);
  ImprimeListaSimples(inicio1);

  if (BuscaElementoListaSimples(inicio1, 20)!=NULL)
    printf("Elemento 20 pertence à lista\n");
  if (BuscaElementoListaSimples(inicio1, 30)==NULL)
    printf("Elemento 30 não pertence à lista\n");

  InsereInicioListaSimples(&inicio2,10);
  ImprimeListaSimples(inicio2);
  inicio3 = ConcatenaListasSimples(inicio2,inicio1);
  ImprimeListaSimples(inicio3);
  DestroyListaSimples(&inicio1);
  DestroyListaSimples(&inicio2);

  InsereElementoListaSimplesOrdenada(&inicio3,5);
  ImprimeListaSimples(inicio3);
  InsereElementoListaSimplesOrdenada(&inicio3,15);
  ImprimeListaSimples(inicio3);
  InsereElementoListaSimplesOrdenada(&inicio3,30);
  ImprimeListaSimples(inicio3);
  DestroyListaSimples(&inicio3);

return 0;
}
